# syntax=docker/dockerfile:1
FROM ubuntu:22.04

LABEL "maintainer"="Nicolas BOURON" \
  "training"="Docker CNC100"

RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1001 cowsay
RUN apt-get update \
  && apt-get install -y cowsay --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

USER cowsay

ENV PATH $PATH:/usr/games

ENTRYPOINT ["cowsay"]
CMD ["Hello everybody !"]